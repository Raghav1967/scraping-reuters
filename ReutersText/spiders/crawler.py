import scrapy
from logging import log
import pandas as pd


class ReutersText(scrapy.Spider):
    name = "ReutersText"
    start_urls = ['https://www.reuters.com']
    df = pd.read_csv('~/scraping-reuters/2007.0_1.0.csv')
    urls = df['url']

    def parse(self, response):

        for url in self.urls:
            try:
                yield scrapy.Request(url=url, callback=self.extract)
            except:
                log("page skipped")

    def extract(self, response):

        row = {}

        row['Url'] = response.url

        meta_data = response.css('body > meta')

        for meta in meta_data:

            if meta.css('::attr(name)').get() == 'DCSext.ContentChannel':
                row['Topic'] = meta.css('::attr(content)').get()

            if meta.css('::attr(name)').get() == 'DCSext.ChannelList':
                row['TopicList'] = meta.css('::attr(content)').get()

            if meta.css('::attr(name)').get() == 'description':
                row['Summary'] = meta.css('::attr(content)').get()

            if meta.css('::attr(name)').get() == 'keywords':
                row['Keywords'] = meta.css('::attr(content)').get()

            if meta.css('::attr(name)').get() == 'analyticsAttributes.topicChannel':
                row['TopicChannel'] = meta.css('::attr(content)').get()

            if meta.css('::attr(name)').get() == 'analyticsAttributes.topicSubChannel':
                row['TopicSubChannel'] = meta.css('::attr(content)').get()

        row['Headline'] = response.css('div.ArticleHeader_content-container > h1::text').extract_first()
        context_list = response.css('div.StandardArticleBody_body > p::text').extract()
        context = ''

        for i in context_list:
            context = context + i

        row['DetailedText'] = context

        yield row
